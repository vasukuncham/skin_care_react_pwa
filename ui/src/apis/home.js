import api from './base';
import { UpdateProducts } from '../store/home/actions';
import { PRODUCT_LIST } from './apis';

export function ProductListAPI() {
        return function (dispatch) {
                api(PRODUCT_LIST)
                .then((resp) => {
                    dispatch(UpdateProducts(resp.results));
                }, (error) => {
                    dispatch(UpdateProducts([]));
                });
        }
}