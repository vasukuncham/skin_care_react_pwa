import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import { ProductListAPI } from '../apis/home';
import Product from '../components/Product';
import Footer from '../components/Footer';
import DemoCarousel from '../components/DemoCarousel';
import Offers from '../components/Offers';
import Snackbar from '@material-ui/core/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles({
  loadmore: {
    width: "100%",
    padding: "20px"
  }
});


function HomePage(props) {
  const classes = useStyles();
  const [spacing] = React.useState(2);
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
    setTimeout(()=>setOpen(false), 3000);
  };

  React.useEffect(() => {
      props.productListAPI();
  }, []);

  const progress = (props.loading)? <CircularProgress align="center"/>: "";
  return (
    <div>
      <DemoCarousel />

      <div className="promotions" align="center">
        <Tooltip title="Pramotions banner">
          <img src="http://lorempixel.com/output/fashion-q-c-640-120-3.jpg" alt="Pramotions banner" />
        </Tooltip>
      </div>

      <div className="spacing" />
      <h3 className="secondary-title">Product List</h3>
      <div className="spacing" /> 

      <Grid item xs={12}>
        <Grid container spacing={spacing}>
          {progress}
          {props.products.map(value => (
            <Grid key={value.id} item xs={6} sm={4} md={3} lg={3}>
              <Product product={value} notify={handleClick} />
            </Grid>
          ))}
          <div align="center" className={classes.loadmore}>
            <Button size="medium" color="secondary" variant="contained">
              LOAD MORE
            </Button>
          </div>
        </Grid>
      </Grid>

      <div className="promotions" align="center">
        <Tooltip title="Pramotions banner">
          <img src="http://lorempixel.com/output/fashion-q-c-640-120-3.jpg" alt="Pramotions banner" />
        </Tooltip>
      </div>

      <div className="spacing" />
      <h3 className="secondary-title">Our New Offers</h3>
      <div className="spacing" /> 

      <Offers />

      <div className="divider"></div>

      <Footer />

      <div className="spacing" />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={open}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">Item has been added to the cart</span>}
      />
    </div>
  );
}


const mapStateToProps = state => ({
  products: state.home.products,
  loading: state.home.loading
})

const mapDispatchToProps = {
  productListAPI: ProductListAPI
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
