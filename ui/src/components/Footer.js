import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
  theme: {
    color: '#FB631C'
  }
}));
 
export default function Footer() {
  const [spacing] = React.useState(4);
  const classes = useStyles();

    return (
      <div>
        <Grid item xs={12} className="main-footer">
          <Grid container spacing={spacing}>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">Perfumes</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">Skincare</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">Accesories</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">Cosmetics</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">Offers</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <div className="footer-div">
                <div className="title">My Account</div>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Concditions</a></li>
              </div>
            </Grid>
          </Grid>
        </Grid>
        <div className="divider"></div>
        <Grid item xs={12}>
          <Grid container spacing={spacing}>
            <Grid item xs={6} sm={6} md={6} lg={6}>
              &copy; 2019 Vasu
            </Grid>
            <Grid item xs={6} sm={6} md={6} lg={6} align="right" className="socialIcons">
              <Tooltip title="Facebook">
                <img src="https://i1.lmsin.net/website_images/static-pages/brand_exp/brand2images/icons/facebook-black-24.svg" alt="fb" />
              </Tooltip>
              <Tooltip title="Twitter">
                <img src="https://i1.lmsin.net/website_images/static-pages/brand_exp/brand2images/icons/twitter-black-24.svg" alt="Twitter" />
              </Tooltip>
              <Tooltip title="Instagram">
              <img src="https://i1.lmsin.net/website_images/static-pages/brand_exp/brand2images/icons/instagram-black-24.svg" alt="Instagram" />
              </Tooltip>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
}
 