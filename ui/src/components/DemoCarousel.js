import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
 
export default function DemoCarousel() {
    return (
        <Carousel 
            autoPlay
            showArrows={true}
            showStatus={false}
            showIndicators={true}
            showThumbs={false}
            dynamicHeight={true} >
            <div>
              <img src="http://lorempixel.com/output/food-q-c-640-300-3.jpg" alt="img" />
              <p className="legend">Image 01</p>
            </div>
            <div>
              <img src="http://lorempixel.com/output/food-q-c-640-300-2.jpg" alt="img"/>
              <p className="legend">Image 02</p>
            </div>
            <div>
              <img src="http://lorempixel.com/output/food-q-c-640-300-1.jpg" alt="img"/>
              <p className="legend">Image 03</p>
            </div>
            <div>
              <img src="http://lorempixel.com/output/food-q-c-640-300-4.jpg" alt="img"/>
              <p className="legend">Image 04</p>
            </div>
            <div>
              <img src="http://lorempixel.com/output/food-q-c-640-300-5.jpg" alt="img"/>
              <p className="legend">Image 05</p>
            </div>
          </Carousel>
    );
}
 