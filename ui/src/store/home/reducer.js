import
{ 
  UPDATE_PRODUCTS
}
from
'../constants';

const initState = {
  loading: true,
  products: []
}

const reducer = (state=initState, action) => {
  switch (action.type) {
    case UPDATE_PRODUCTS:
      return {
        ...state,
        products: action.payload,
        loading: false
      }
    default:
      return state
  }
}


export default reducer;