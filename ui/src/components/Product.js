import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';

import { connect } from 'react-redux';
import {UpdateCartProducts} from '../store/cart/actions';

const useStyles = makeStyles({
  basicButton: {
    backgroundColor: "#FB631C",
    color: "#FFF",
    width: '100%'
  },
});

function Product(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(2);

  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Product Image"
          image="https://i.postimg.cc/rRx06KBj/contemplative-reptile.jpg"
          title="Product Image"
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h6">
            {props.product.name}
          </Typography>
          <Typography variant="body2" className="desc" color="textSecondary" component="p">
            sample description about the product.
          </Typography>
          <Typography variant="body1" color="textSecondary" component="p">
            ${props.product.price}
          </Typography>
          <div>
            <Rating
              name="simple-controlled"
              value={value}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
            />
          </div>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="secondary" variant="contained" className={classes.basicButton}
          onClick = { () => {
              props.updateCartProducts(props.product);
              props.notify(true);
          }}>
          Add to Cart
        </Button>
      </CardActions>
    </Card>
  );
}


const mapStateToProps = state => ({
  cart: state.cart.products
})
const mapDispatchToProps = dispatch => ({
  updateCartProducts: (data) => dispatch(UpdateCartProducts(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Product);