import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
// import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import DoneOutline from '@material-ui/icons/DoneOutline';

const useStyles = makeStyles({
  basicCard: {
    padding: "20px",
    width: '100%',
    textAlign: 'center'
  },
  succIcon: {
    color: 'green'
  },
  title: {
    fontSize: '22px',
    fontWeight: '600',
    color: 'green',
    padding: '10px 0px'
  },
  goHome: {
    marginTop: '20px',
    backgroundColor: "#FB631C"
  }
});

export default function Success() {
  const classes = useStyles();
  const [spacing] = React.useState(2);

  return (
    <div>
      <Grid item xs={12}>
        <Grid container spacing={spacing}>
          <Card className={classes.basicCard}>
            <div><DoneOutline className={classes.succIcon} fontSize="large" /></div>
            <div className={classes.title}>
              Your order has been placed successfully.
            </div>
            <div className={classes.subText}>
              Order ID #7984192817931827
            </div>
            <Link to="/">
              <Button 
                variant="contained" 
                size="small" 
                className={classes.goHome} 
                align="right"
                color="secondary"
              >Continue Shopping</Button>
            </Link>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}
