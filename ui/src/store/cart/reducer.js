import
{ 
  UPDATE_CART_PRODUCTS,
  PLACE_ORDER,
  REMOVE_CART_PRODUCT
}
from
'../constants';

const initState = {
  loading: false,
  products: [],
  order: {}
}

const reducer = (state=initState, action) => {
  switch (action.type) {
    case UPDATE_CART_PRODUCTS:
      return {
        ...state,
        products: [...state.products, action.payload]
      }
    case PLACE_ORDER:
      return {
        ...state,
        order: action.payload,
        products: []
      }
    case REMOVE_CART_PRODUCT:
      return {
        ...state,
        products: state.products.filter((product)=>action.payload!==product.id)
      }
    default:
      return state
  }
}


export default reducer;