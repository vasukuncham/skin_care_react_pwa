const express = require('express');
const router = express.Router();

const products = require('./products.json');
let orders = [];

// @route GET api/v1/products/
// @desc get product list
// @access Public
router.get('/products', function (req, res) {

	res.status(200).send({status: 0, 'results': products.products});
});


// @route GET api/v1/order/
// @desc create order api
// @access Public
router.post('/order', function (req, res) {

	let order = {
		orderID: Date.now(),
		order_details: req.body,
		orderDate: new Date()
	};
    orders.push(order);
	res.status(200).send({status: 0, 'results': "Success"});
});


module.exports = router;
