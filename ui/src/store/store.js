import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import home from './home/reducer';
import cart from './cart/reducer';

const reducer = combineReducers({
  home,
  cart
})

function configureStore(state = {}) {
  return createStore(reducer, applyMiddleware(thunk));
}
export default configureStore;