import axios from 'axios';
  
export default function api(url, method='GET', data={}) {

        return new Promise(function(resolve, reject) {
            const requestOptions = {
                url: url,
                method: method,
                crossDomain: true,
                headers: {
                        'Content-Type': 'application/json'
                },
                data
            };

            axios(requestOptions)
                .then(function (response) {
                        resolve(response.data);
                })
                .catch(function (error) {
                        reject(error);
                });
        });
}