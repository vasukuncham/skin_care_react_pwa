const express = require('express');
const bodyParser = require('body-parser');

const router = require('./router');

const app = express();
const port = 3002;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

  if ('OPTIONS' === req.method) {
    res.send(200);
  } else {
    next();
  }
});

//add routes
const base = '/api/v1/';
app.use(base, router);

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
