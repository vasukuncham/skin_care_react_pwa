// Home page

export const UPDATE_PRODUCTS = 'UPDATE_PRODUCTS';


export const UPDATE_CART_PRODUCTS = 'UPDATE_CART_PRODUCTS';
export const REMOVE_CART_PRODUCT = 'REMOVE_CART_PRODUCT';

export const PLACE_ORDER = 'PLACE_ORDER';