import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AddShoppingCartOutlinedIcon from '@material-ui/icons/AddShoppingCartOutlined';
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import '../css/header.scss';

import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  colorWhite: {
    color: '#fff'
  }
}));

function AppHeader(props) {
  const classes = useStyles();
  const productsCount = props.products.length;

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link to="/" className={classes.colorWhite}>SkinCare</Link>
          </Typography>
          <Tooltip title="Your Cart">
            <Link to="/cart">
                <AddShoppingCartOutlinedIcon className={classes.colorWhite} />
                <span className="cartCount">{productsCount}</span>
            </Link>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = state => ({
  products: state.cart.products
})

export default connect(mapStateToProps)(AppHeader);