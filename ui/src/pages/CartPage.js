import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {Card, Typography, Divider} from '@material-ui/core';
import { Link } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';

import { connect } from 'react-redux';
import { PlaceOrder, RemoveCartProduct } from '../store/cart/actions';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  card: {
    padding: '20px'
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  itemTitle: {
    fontSize: "20px"
  },
  itemDesc: {
    color: "grey",
    fontSize: "14px"
  },
  itemPriceLabel: {
    color: "grey",
    fontSize: "14px"
  },
  itemPrice: {
    display: "inline-flex",
    alignItems: "center"
  },
  itemPriceValue: {
    fontSize: "18px",
    fontWeight: "600",
    paddingLeft: "10px"
  },
  totalPriceValue: {
    fontSize: "18px",
    fontWeight: "600"
  },
  avatar: {
    borderRadius: 0,
    marginRight: "14px",
    width: "80px",
    height: "70px"
  },
  image: {
    width: "100%",
    height: "100%"
  },
  placeOrder: {
    backgroundColor: "#FB631C",
    marginTop: "15px",
    color: "#FFF",
    width: '100%'
  },
  textField: {
    width: "100%"
  },
  goHome: {
    marginTop: '20px',
    backgroundColor: "#FB631C",
    float: 'right'
  },
  textStyles: {
    lineHeight: '24px'
  }
}));

function Greeting(props) {
    const classes = useStyles();
    /*const [dense] = React.useState(false);
    const [secondary] = React.useState(false);*/
    const [spacing] = React.useState(2);

    if (props.products.length > 0) {

      return <div>{props.products.map(value => ( 
              <Grid item xs={12}>
                <Grid container spacing={spacing}>
                  <Grid item xs={12} sm={3} md={2} lg={2}>
                    <Avatar className={classes.avatar}>
                      <img 
                      className={classes.image}
                      alt="img"
                      src="https://i.postimg.cc/rRx06KBj/contemplative-reptile.jpg" />
                    </Avatar>
                  </Grid>
                  <Grid item xs={10} sm={7} md={9} lg={9} className={classes.textStyles}>
                    <div className={classes.itemTitle}>{value.name}</div>
                    <div className={classes.itemDesc}>Sample Product description</div>
                    <div className={classes.itemDesc}>Seller: VoxDigital</div>
                    <div className={classes.itemPrice}>
                      <div className={classes.itemPriceLabel}>Price:</div>
                      <div className={classes.itemPriceValue}>${value.price}</div>
                    </div>
                  </Grid>
                  <Grid item xs={2} sm={2} md={1} lg={1}>
                    <Tooltip title="Remove Item" placement="top-start">
                      <IconButton edge="end" aria-label="delete" onClick={()=>props.removeCartProduct(value.id)}>
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
                <br/>
                  <Divider />
                <br/>
              </Grid> ))}</div>;
    }
    return <p align="center">Your cart is currently empty</p>;
}

function CartPage(props) {
  const [spacing] = React.useState(2);
  const [dense] = React.useState(false);
  const [address, setAddress] = React.useState("");
  const classes = useStyles();
  const productsCount = props.products.length;
  let totalPrice = 0;
  if (productsCount) {
    totalPrice = props.products.reduce((sum, {price})=>sum + parseInt(price), 0);
  }

  const handleChange = (event) => {
    setAddress(event.target.value);
  };

  const placeOrder = () => {
    let data = {
      address,
      products: props.products,
      productsCount,
      totalPrice
    }
    props.placeOrder(data);
    setAddress("");
    props.history.push("/success");
  }

  return (
    <Grid item xs={12}>
      <Grid container spacing={spacing}>
        <Grid item xs={12} sm={8} md={8} lg={8}>
          <Card className={classes.card}>
            <Typography variant="h6" component="h5">My Cart</Typography>
            <hr />
            <Grid item xs={12} md={12}>
              <div className={classes.demo}>
                <Greeting products={props.products} removeCartProduct={props.removeCartProduct}/>
              </div>
            </Grid>
          </Card>
          <Link to="/" align="center">
            <Button 
              variant="contained" 
              size="medium" 
              className={classes.goHome} 
              align="right"
              color="secondary"
            >Continue Shopping</Button>
          </Link>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Card className={classes.card}>
            <Typography variant="h6" component="h5">Price Details</Typography>
            <hr />
            <Grid item xs={12} md={12}>
              <div className={classes.demo}>
                <List dense={dense}>
                  <ListItem>
                    <ListItemText
                    >
                      <div>Items({productsCount})</div>
                    </ListItemText>
                    <ListItemSecondaryAction>
                      ${totalPrice}
                    </ListItemSecondaryAction>
                  </ListItem>
                  <ListItem>
                    <ListItemText
                    >
                      <div>Delivery</div>
                    </ListItemText>
                    <ListItemSecondaryAction>
                      FREE
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem>
                    <ListItemText>
                        <div className={classes.totalPriceValue}>Total</div>
                      </ListItemText>
                      <ListItemSecondaryAction className={classes.totalPriceValue}>
                        ${totalPrice}
                      </ListItemSecondaryAction>
                  </ListItem>
                </List>
              </div>
            </Grid>
          </Card>

          <TextField
            id="outlined-multiline-flexible"
            label="Enter Address"
            multiline
            rowsMax="5"
            value={address}
            onChange={handleChange}
            className={classes.textField}
            margin="normal"
            variant="outlined"
          />

          <Button 
            variant="contained" 
            size="large" 
            className={classes.placeOrder} 
            align="right"
            color="secondary"
            disabled={!address || !productsCount}
            onClick={placeOrder}
          >
            Place Order
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => ({
  products: state.cart.products
})

const mapDispatchToProps = dispatch => ({
  placeOrder: (data) => dispatch(PlaceOrder(data)),
  removeCartProduct: (data) => dispatch(RemoveCartProduct(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);