import
{ 
	UPDATE_PRODUCTS
}
from
'../constants';

export const UpdateProducts = (payload) => ({
  type: UPDATE_PRODUCTS,
  payload: payload
})
