import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AddShoppingCartOutlinedIcon from '@material-ui/icons/AddShoppingCartOutlined';

const useStyles = makeStyles(theme => ({
  theme: {
    color: '#FB631C'
  }
}));
 
export default function Offers() {
  const [spacing] = React.useState(4);
  const classes = useStyles();

    return (
        <Grid item xs={12}>
          <Grid container spacing={spacing}>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <div className="offers-div">
                <div className="offer-icon"><AddShoppingCartOutlinedIcon className={classes.theme} /></div>
                <div className="offer-title">
                  FREE SHIPPING
                </div>
                <div className="offer-desc">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor minim veniam, quis nostrud reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
                </div>
              </div>
            </Grid>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <div className="offers-div">
                <div className="offer-icon"><AddShoppingCartOutlinedIcon className={classes.theme} /></div>
                <div className="offer-title">
                  100% REFUND
                </div>
                <div className="offer-desc">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor minim veniam, quis nostrud reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
                </div>
              </div>
            </Grid>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <div className="offers-div">
                <div className="offer-icon"><AddShoppingCartOutlinedIcon className={classes.theme} /></div>
                <div className="offer-title">
                  SUPPORT 24/7
                </div>
                <div className="offer-desc">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor minim veniam, quis nostrud reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
                </div>
              </div>
            </Grid>
          </Grid>
        </Grid>
    );
}
 