import React from 'react';
import logo from './logo.svg';
import './css/App.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Container from '@material-ui/core/Container';

import AppHeader from './components/AppHeader';
import HomePage from './pages/HomePage';
import CartPage from './pages/CartPage';
import Success from './pages/Success';

function App() {
  return (
    <div className="App">
      <Router>
        <AppHeader />
        <div className="main-container">
          <Container>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/cart" component={CartPage} />
            <Route exact path="/success" component={Success} />
          </Container>
        </div>
      </Router>
    </div>
  );
}

export default App;
