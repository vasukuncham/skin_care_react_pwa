import
{ 
	UPDATE_CART_PRODUCTS,
	PLACE_ORDER,
	REMOVE_CART_PRODUCT
}
from
'../constants';

export const UpdateCartProducts = (payload) => ({
  type: UPDATE_CART_PRODUCTS,
  payload: payload
})

export const RemoveCartProduct = (payload) => ({
  type: REMOVE_CART_PRODUCT,
  payload: payload
})

export const PlaceOrder = (payload) => ({
  type: PLACE_ORDER,
  payload: payload
})


